import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { GitlabService } from '@core/services/gitlab.service';
import { Project } from '@core/models/project';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @Output() searchFile = new EventEmitter<string>();

  projects: Project[];
  searchFileName: string;

  constructor(
    private gitlabService: GitlabService
  ) { }

  ngOnInit(): void {
    this.gitlabService.getProjects().subscribe({
      next: (projects: Project[]) => {
        this.projects = projects;
      },
      error: () => {
        this.projects = [];
      }
    });
  }

  onSearchFile(fileName): void {
    this.searchFileName = fileName;
  }

}
