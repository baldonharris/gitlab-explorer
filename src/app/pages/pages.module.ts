import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@core/core.module';
import { HomeComponent } from './home/home.component';
import { SharedModule } from '@shared/shared.module';
import { AboutComponent } from './about/about.component';



@NgModule({
  declarations: [
    HomeComponent,
    AboutComponent
  ],
  imports: [
    CommonModule,
    CoreModule,
    SharedModule
  ],
  exports: [
    HomeComponent,
    AboutComponent
  ]
})
export class PagesModule { }
