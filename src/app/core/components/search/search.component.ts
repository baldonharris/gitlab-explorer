import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @Output() searchFile = new EventEmitter<string>();

  onKeyUpTimer: any;

  constructor() { }

  ngOnInit(): void {
  }

  onKeyUp(fileName: string): void {
    clearTimeout(this.onKeyUpTimer);
    this.onKeyUpTimer = setTimeout(() => {
      this.searchFile.emit(fileName);
    }, 500);
  }

}
