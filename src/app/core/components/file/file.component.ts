import { Component, Input, OnInit } from '@angular/core';
import { File } from '@core/models/file';
import { Branch } from '@core/models/branch';
import { GitlabService } from '@core/services/gitlab.service';
import { Subscription } from 'rxjs';
import { FileCommit } from '@core/models/file-commit';
import { CommitDetail } from '@core/models/commit-detail';

@Component({
  selector: 'app-file',
  templateUrl: './file.component.html',
  styleUrls: ['./file.component.css']
})
export class FileComponent implements OnInit {
  @Input() projectId: number;
  @Input() file: File;
  @Input() selectedBranch: Branch;

  fileCommitSubscription: Subscription;
  commitDetailSubscription: Subscription;
  commitDetail: CommitDetail;
  fileTypeIcons = {
    tree: 'bi bi-folder',
    blob: 'bi bi-file-code'
  };
  timerToFetch: any;

  constructor(
    private gitlabService: GitlabService
  ) { }

  ngOnInit(): void {
  }

  onMouseOver(popover: any, fileType: string): void {
    if (fileType !== 'tree') {
      this.timerToFetch = setTimeout(() => {
        popover.open();
        this.fileCommitSubscription = this.gitlabService
          .getFileCommit(this.projectId, this.file.path.replace(/\//g, '%2F'), this.selectedBranch.name)
          .subscribe({
            next: (fileCommit: FileCommit) => {
              this.commitDetailSubscription = this.gitlabService
                .getCommitDetail(this.projectId, fileCommit.last_commit_id)
                .subscribe({
                  next: (commitDetail: CommitDetail) => {
                    this.commitDetail = commitDetail;
                  },
                  error: () => {
                    this.commitDetail.id = 'ERROR';
                    this.commitDetail.message = 'ERROR: An error occurred';
                  },
                  complete: () => {
                    popover.open({ windowClass : 'myCustomModalClass'});
                  },
                });
            }
          });
      }, 300);
    }
  }

  onMouseOut(popover: any): void {
    if (!!this.fileCommitSubscription) {
      this.fileCommitSubscription.unsubscribe();
    }

    if (!!this.commitDetailSubscription) {
      this.commitDetailSubscription.unsubscribe();
    }

    if (this.timerToFetch) {
      clearTimeout(this.timerToFetch);
    }

    popover.close();
  }

}
