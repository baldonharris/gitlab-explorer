import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Project } from '@core/models/project';
import { GitlabService } from '@core/services/gitlab.service';
import { File } from '@core/models/file';
import { Branch } from '@core/models/branch';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit, OnChanges {
  @Input() project: Project;
  @Input() searchFileName: string;

  files: File[];
  searchedFiles: File[];
  selectedBranch: Branch;
  paginated: File[];

  constructor(
    private gitlabService: GitlabService
  ) {
  }

  ngOnInit(): void {
    this.paginated = [];
    this.gitlabService.getProjectBranches(this.project.id).subscribe({
      next: (branches: Branch[]) => {
        this.project.branches = branches;
      },
      error: () => {
        this.project.branches = [];
      },
      complete: () => {
        if (this.project.branches.length > 0) {
          this.fetchFiles(this.project.branches[0].name);
          this.selectedBranch = this.project.branches[0];
        } else {
          this.files = [];
        }
      }
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((!!this.files && this.files.length > 0) && changes.searchFileName.currentValue !== changes.searchFileName.previousValue) {
      this.searchFiles(changes.searchFileName.currentValue.toLowerCase());
    }
  }

  searchFiles(fileNameToSearch: string): void {
    if (fileNameToSearch) {
      this.searchedFiles = this.files.filter((file: File) => {
        return file.name.toLowerCase().indexOf(fileNameToSearch) > -1 && file.type !== 'tree';
      });
    } else {
      this.searchedFiles = undefined;
    }
  }

  fetchFiles(branchName?: string, cb?: () => void): void {
    branchName = !!branchName ? branchName : null;
    this.files = undefined;
    this.selectedBranch = undefined;

    this.gitlabService.getRecursiveFiles(this.project.id, 1, branchName).subscribe({
      next: (files: any) => {
        const totalPages = Number(files.headers.get('x-total-pages'));
        this.files = files.body;

        for (let page = 2; page <= totalPages; page++) {
          this.gitlabService.getRecursiveFiles(this.project.id, page, branchName).subscribe({
            next: (moreFiles: any) => {
              this.files = this.files.concat(moreFiles.body);
            },
            complete: () => {
              if (page === totalPages && !!cb) {
                cb();
              }
            }
          });
        }
      },
      error: () => {
        this.files = [];
      }
    });
  }

  onSelectBranch(branch: Branch): void {
    this.searchedFiles = undefined;
    this.fetchFiles(branch.name, () => {
      this.searchFiles(this.searchFileName);
    });
    this.selectedBranch = branch;
  }

  onPaginate(paginated: File[]): void {
    setTimeout(() => {
      this.paginated = paginated;
    }, 100);
  }

}
