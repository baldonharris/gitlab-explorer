import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { GitlabInterceptor } from '@core/interceptors/gitlab.interceptor';
import { ProjectComponent } from './components/project/project.component';
import { FileComponent } from './components/file/file.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SearchComponent } from './components/search/search.component';



@NgModule({
  declarations: [
    ProjectComponent,
    FileComponent,
    SearchComponent
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GitlabInterceptor,
      multi: true
    }
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    SharedModule,
    NgbModule
  ],
  exports: [
    ProjectComponent,
    FileComponent,
    SearchComponent
  ]
})
export class CoreModule { }
