import { TestBed } from '@angular/core/testing';

import { GitlabInterceptor } from './gitlab.interceptor';

describe('GitlabInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      GitlabInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: GitlabInterceptor = TestBed.inject(GitlabInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
