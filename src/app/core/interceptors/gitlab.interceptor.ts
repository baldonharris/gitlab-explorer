import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpParams
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment as env } from '@env';

@Injectable()
export class GitlabInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let params = request.params;
    params = params.append('private_token', env.gitlab.accessToken);

    const gitlabReq = request.clone({url: `${env.gitlab.url}${request.url}`, params});

    return next.handle(gitlabReq);
  }
}
