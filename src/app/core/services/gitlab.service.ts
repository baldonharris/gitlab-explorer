import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '@core/models/user';
import { Observable } from 'rxjs';
import { environment as env } from '@env';
import { Project } from '@core/models/project';
import { Branch } from '@core/models/branch';
import { FileCommit } from '@core/models/file-commit';
import { CommitDetail } from '@core/models/commit-detail';

@Injectable({
  providedIn: 'root'
})
export class GitlabService {

  constructor(
    private http: HttpClient
  ) { }

  getUser(userId?: number): Observable<User> {
    userId = !!userId ? userId : env.gitlab.user;

    return this.http.get<User>( `users/${userId}`);
  }

  getProjects(userId?: number): Observable<Project[]> {
    userId = !!userId ? userId : env.gitlab.user;

    return this.http.get<Project[]>(`users/${userId}/projects`);
  }

  getProjectBranches(projectId: number): Observable<Branch[]> {
    return this.http.get<Branch[]>(`projects/${projectId}/repository/branches`);
  }

  getRecursiveFiles(projectId: number, page?: number, branchName?: string): Observable<any> {
    let params = new HttpParams();
    params = params.appendAll({
      recursive: 'true',
      per_page: '100'
    });

    if (page) {
      params = params.append('page', `${page}`);
    }

    if (branchName) {
      params = params.append('ref', branchName);
    }

    return this.http.get<any>(`projects/${projectId}/repository/tree`, {params, observe: 'response'});
  }

  getFileCommit(projectId: number, path: string, branch: string): Observable<FileCommit> {
    let params = new HttpParams();
    params = params.append('ref', branch);

    return this.http.get<FileCommit>(`projects/${projectId}/repository/files/${path}`, {params});
  }

  getCommitDetail(projectId: number, commitId: string): Observable<CommitDetail> {
    return this.http.get<CommitDetail>(`projects/${projectId}/repository/commits/${commitId}`);
  }

}
