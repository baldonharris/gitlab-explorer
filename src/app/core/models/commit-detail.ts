export interface CommitDetail {
  id: string;
  message: string;
  committer_name: string;
  committed_date: string;
}
