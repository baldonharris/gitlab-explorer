import { Branch } from '@core/models/branch';

export interface Project {
  id: number;
  name_with_namespace: string;
  web_url: string;
  branches: Branch[];
  visibility: 'private'|'public';
}
