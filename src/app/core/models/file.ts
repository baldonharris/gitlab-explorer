export interface File {
  id: string;
  mode: string;
  name: string;
  path: string;
  type: string;
}
