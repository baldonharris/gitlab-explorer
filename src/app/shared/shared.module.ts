import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from './components/loader/loader.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { SearchFileComponent } from './components/search-file/search-file.component';
import { NoFileFoundComponent } from './components/no-file-found/no-file-found.component';
import { EmptyProjectComponent } from './components/empty-project/empty-project.component';



@NgModule({
  declarations: [
    LoaderComponent,
    PaginationComponent,
    SearchFileComponent,
    NoFileFoundComponent,
    EmptyProjectComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LoaderComponent,
    PaginationComponent,
    SearchFileComponent,
    NoFileFoundComponent,
    EmptyProjectComponent
  ]
})
export class SharedModule { }
