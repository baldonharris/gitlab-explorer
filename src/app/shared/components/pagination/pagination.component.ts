import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() arrToPaginate: any[];
  @Input() perPage: number;

  @Output() slicedArr = new EventEmitter<any>();

  paginated: any[];
  maxButtons = 5;
  currentPage = 0;
  paginationPages: any[];

  constructor() { }

  static chunk(array, size): any {
    const result = [];
    for (const value of array){
      const lastArray = result[result.length - 1];
      if (!lastArray || lastArray.length === size){
        result.push([value]);
      } else{
        lastArray.push(value);
      }
    }

    return result;
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.arrToPaginate) {
      this.paginated = PaginationComponent.chunk(this.arrToPaginate, this.perPage);
      this.slicedArr.emit(this.paginated.length > 0 ? this.paginated[0] : []);
      this.currentPage = 0;

      this.generatePages();
    }
  }

  generatePages(startFrom?: number): void {
    if (this.paginated.length > this.maxButtons) {
      const maxBtnWatcher = (startFrom - 1) + this.maxButtons;
      if (maxBtnWatcher > this.paginated.length) {
        startFrom = startFrom - (maxBtnWatcher - this.paginated.length);
      }
    }

    this.paginationPages = Array.from(
      {length: this.paginated.length < this.maxButtons ? this.paginated.length : this.maxButtons},
      (_, i) => i + (!!startFrom ? (startFrom - 1) : 0)
    );
  }

  goToPage(page: number): void {
    this.slicedArr.emit(this.paginated[page]);
    this.currentPage = page;

    const refPage = page + 1;
    if (refPage > 2 && this.paginated.length > this.maxButtons) {
      this.generatePages(refPage - 2);
    } else {
      this.generatePages();
    }
  }

}
