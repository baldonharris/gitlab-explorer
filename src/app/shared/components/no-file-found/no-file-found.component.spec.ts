import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoFileFoundComponent } from './no-file-found.component';

describe('NoFileFoundComponent', () => {
  let component: NoFileFoundComponent;
  let fixture: ComponentFixture<NoFileFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoFileFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoFileFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
