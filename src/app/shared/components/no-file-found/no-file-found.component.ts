import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-file-found',
  templateUrl: './no-file-found.component.html',
  styleUrls: ['./no-file-found.component.css']
})
export class NoFileFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
