# GitLab Explorer

A project that can search user's files and it's last commit information using [GitLab's API](https://docs.gitlab.com/ee/api/).

## Development server

Prerequisites:
1. nodejs v10+
2. npm
3. [angular cli](https://angular.io/cli)

- Run `ng serve` for a dev server.
- Navigate to `http://localhost:4200/`.

Changing user:
- Change `environment.gitlab.user` in `src/environments/environment.ts` with the GitLab User ID.
- Optional: Change also the same property in `src/environments/environment.production.ts` if the User ID will be used in production.

## Code scaffolding

- core - module where project core `component|directive|pipe|service|interface` are stored.
- pages - module where project pages are stored.
- shared - module where common `components` should be stored.

## Build

- Run `ng build` to build the project.
- The build artifacts will be stored in the `dist/` directory.
- Use the `--prod` flag for a production build.

## Run in production

Prerequisites:
1. docker
2. docker-compose
3. Built project

- Run `docker-compose up -d`
- Access server's url in browser

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
